const express = require('express');
const router = express.Router();

const housesController = require('../controllers/houses.controller');

router.get('/', housesController.getHouses);
router.post('/register', housesController.registerHouse);
router.get('/edit/:house_id', housesController.getEditingHouse);
router.post('/edit', housesController.editHouse);
router.get('/delete/:house_id', housesController.deleteHouse);

module.exports = router
