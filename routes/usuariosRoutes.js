const express = require('express');
const router = express.Router();
const usuarios_controller = new(require('../controllers/usuariosController')).UsuariosController();

router.get('/', usuarios_controller.getAll)
router.post('/', usuarios_controller.create)
router.patch('/:usuario_id', usuarios_controller.update)
router.delete('/:usuario_id', usuarios_controller.delete)
router.get('/state', usuarios_controller.get_state_enum)

module.exports = router