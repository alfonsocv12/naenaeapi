const express = require('express');
const router = express.Router();
const actividades_controller = new (require('../controllers/actividadesController')).ActividadesController();

router.get('/',actividades_controller.getAll)
router.post('/',actividades_controller.create)
router.patch('/:actividad_id',actividades_controller.update)
router.delete('/:actividad_id',actividades_controller.delete)
router.get('/state', actividades_controller.get_state_enum)

module.exports = router
