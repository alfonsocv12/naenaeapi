const express = require('express');
const router = express.Router();
const pagos_controller = new (require('../controllers/pagosController')).PagosController()

router.get('/', pagos_controller.getAll);
router.post('/', pagos_controller.create);
router.patch('/:pago_id/:user_id', pagos_controller.update);
router.delete('/', pagos_controller.delete);
router.get('/usuario/:user_id', pagos_controller.get_user_pagos);
router.get('/estados_pago', pagos_controller.get_estados_pago)

module.exports = router
