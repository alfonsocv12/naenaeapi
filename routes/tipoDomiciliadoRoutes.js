const express = require('express');
const router = express.Router();
const tipo_domicialiado_controller = new (require("../controllers/TipoDomiciliadoController")).TipoDomiciliadoController();

router.get('/', tipo_domicialiado_controller.getAll);

module.exports = router
