const Actividad = require('../models/activityModel');
const ObjectId = require('mongoose').Types.ObjectId;

exports.ActividadesController = class ActividadesClass {
  /*Funcion constructora*/
  constructor() {
    require('auto-bind')(this);
  }

  /*Funcion que regresa todas
  las actividades*/
  async getAll(req, res){
    try{
      res.status(200).json(await Actividad.find());
    } catch(error){
      res.status(400).json(error);
    }
  }

  /*Funcion encargada de crear
  una actividad*/
  async create(req, res){

    try{
      const actividad = new Actividad(req.body);
      (!req.body.user_id) ? actividad.user_id = ObjectId() : null;
      await actividad.save();
      res.status(200).json(actividad);
    } catch(error){
      res.status(400).json(error);
    }
  }

  /*Funcion encargada de actualizar
  las actividades*/
  async update(req, res){
    try{
      let actividad = await Actividad.findById(req.params.actividad_id);
      actividad = this.set_activity_data(actividad ,req.body);
      await actividad.save()
      res.status(200).json(actividad);
    } catch(error){
      res.status(400).json(error);
    }
  }

  /*Funcion encargada de poner los
  datos segun el body*/
  set_activity_data(actividad, body){
    (body.name) ? actividad.name = body.name : null;
    (body.imgsrc) ? actividad.imgsrc = body.imgsrc : null;
    (body.state) ? actividad.state = body.state : null;
    (body.description) ? actividad.description = body.description : null;
    (body.capacity) ? actividad.capacity = body.capacity : null;
    (body.peopleAttending) ? actividad.peopleAttending = actividad.peopleAttending.push(ObjectId(body.peopleAttending)) : null;
    return actividad
  }

  /*Funcion encargada de borrar
  actividades*/
  async delete(req, res){
    try{
      let respuesta = await Actividad.findOneAndRemove({_id:req.params.actividad_id});
      res.status(200).json(respuesta);
    } catch(error){
      res.status(500).json(error);
    }
  }

  /*Funcion encargada de regresar
  los estados de las actividades*/
  get_state_enum(req, res){
    res.status(200).json(Activida.schema.obj.state.enum);
  }
}
