const House = require('../models/house.model');

exports.getHouses = (req, res) => {
    House.find({}, function(err, houses) {
        if(err) throw(err);

        res.status(200).json(houses);
    });
}

exports.registerHouse = (req, res) => {
    const newHouse = new House({
        street: req.body.street,
        exteriorNumber: req.body.exteriorNumber
    }).save();

    res.status(200).json(newHouse);
}

exports.getEditingHouse = (req, res) => {
    House.findOne({ _id: req.params.house_id }, function(err, houseToEdit) {
        if(err) throw(err);

        res.status(200).json(houseToEdit);
    });  
}

exports.editHouse = (req, res) => {
    House.findByIdAndUpdate(req.body._id, {
        street: req.body.street,
        exteriorNumber: req.body.exteriorNumber
    }, function(err, newHouse) {
        if(err) throw(err);

        res.status(200).json(newHouse);
    });
}

exports.deleteHouse = (req, res) => {
    House.findByIdAndDelete(req.params.house_id, function(err, callback) {
        if(err) throw(err);

        res.status(200).json(callback);
    });
}