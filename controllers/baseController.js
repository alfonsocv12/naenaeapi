exports.BaseController = class BaseClass {
  /*Funcion constructora*/
  constructor() {
    require('auto-bind')(this);
  }
  /*Funcion encargada de regresar
  que la api esta funcionando*/
  landing(req, res){
    res.send('<h1>It Works!</h1>');
  }

  validator(req, error, value){
    if(!value){
      req.error = error
      throw new Error();
    }
  }
}
