const Usuario = require('../models/userModel');
const ObjectId = require('mongoose').Types.ObjectId;

exports.UsuariosController = class UsuariosClass {
    /*Funcion constructora*/
    constructor() {
        require('auto-bind')(this);
    }

    async getAll(req, res) {
        try {
            res.status(200).json(await Usuario.find());
        } catch (error) {
            res.status(400).json(error);
        }
    }

    async create(req, res) {
        try {
            const usuario = new Usuario(req.body);
            (!req.body.user_id) ? usuario.user_id = ObjectId(): null;
            await usuario.save();
            res.status(200).json(usuario);
        } catch (error) {
            res.status(400).json(error);

        }
    }

    async update(req, res) {
        try {
            let usuario = new Usuario(req.params.usuario_id);
            usuario = this.set_activity_data(usuario, req.body);
            res.status(200).json({ msg: 'Exito' });
        } catch (error) {
            res.status(400).json(error);
        }
    }

    set_user_data(usuario, body) {
        (body.name) ? usuario.name = body.name: null;
        (body.lastname) ? usuario.lastname = body.lastname: null;
        (body.username) ? usuario.username = body.username: null;
        (body.usertype) ? usuario.usertype = body.usertype: null;
        (body.email) ? usuario.email = body.email: null;
    }

    delete(req, res) {

    }

    get_state_enum(req, res) {
        res.status(200).json(Usuario.schema.obj.state.enum);
    }
}