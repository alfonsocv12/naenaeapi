const TipoDomiciliado = require('../models/tipoDomiciliadoModel')

exports.TipoDomiciliadoController = class TipoDomiciliadoClass {
  /*Funcion constructora*/
  constructor() {
    require('auto-bind')(this)
  }

  /*Funcion encargada de regresar
  todos los tipos de domiciliados*/
  async getAll(req, res){
    try{
      res.status(200).json(await TipoDomiciliado.find());
    } catch(error){
      res.status(500).json(error);
    }
  }
}
