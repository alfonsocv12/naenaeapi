const Payment = require("../models/paymentModel");
const base_controller = new (require('./baseController')).BaseController();
const ObjectId = require('mongoose').Types.ObjectId;

exports.PagosController = class PagosClass {
  /*Funcion constructora*/
  constructor() {
    require('auto-bind')(this);
  }

  /*Funcion encargada de regresar
  todos los pagos*/
  async getAll(req,res){
    try{
        res.status(200).json( await Payment.find());
    } catch(error){
      res.status(500).json(error)
    }
  }

  /*Funcion encargada de crear
  pagos*/
  async create(req,res){
    // try{
        const payment = new Payment(req.body);
        (!req.body.user)? payment.users = [{user_id:ObjectId(),imgsrc:null,state:'Por pagar'}] : payment.users = this.users_to_objectid(req.body.user)
        console.log(payment.users);
        await payment.save()
        res.status(200).json(payment)
    // } catch(error) {
    //   res.status(400).json(error)
    // }
  }

  /*Funcion encargada de regresar los
  users id como ObjectId*/
  users_to_objectid(users){
    if(Array.isArray(users)){
      for(let key in users){
        users[key] = {user_id:ObjectId(users[key]), imgsrc:null, state:'Por pagar'};
      }
    } else{
      users = [{user_id:ObjectId(users), imgsrc:null, state:'Por pagar'}]
      console.log(users);
    }
    return users
  }

  /*Funcion encargada de actualizar
  los pagos*/
  async update(req,res){
    try{
      let payment = await Payment.findById(req.params.pago_id);
      payment.users = this.set_pagos_data(req.params.user_id, req.body, payment.users)
      await payment.save()
      res.status(200).json(payment);
    } catch(error){
      res.status(400).json(error)
    }
  }

  /*Funcion encargada da regresar
  los pagos con los datos adecuados*/
  set_pagos_data(user_id, body, users){
    for(let key in users){
      if(users[key].user_id == user_id){
        (body.state)? users[key].state = body.state : null;
        (body.imgsrc)? users[key].imgsrc = body.imgsrc : null;
      }
    }
    return users
  }

  /*Funcion encargada de regresar los
  pagos de un usuario*/
  async get_user_pagos(req, res){
    try{
      let query = this.set_pagos_user(req.params, req.query);
      res.status(200).json(await Payment.aggregate(query))
    } catch(error){
      res.status(500).json(error)
    }
  }

  /*Funcion encargada de regresar
  el query de pagos*/
  set_pagos_user(params, query_req){
    let query = {};
    query["users.user_id"] = ObjectId(params.user_id);
    query_req.state ? query["users.state"] = query_req.state : null;
    return [{$unwind:"$users"},{$match:query}]
  }

  /*Funcion encargada de borrar
  los pagos*/
  delete(req,res){

  }

  /*Funcion encargada de regresar
  los estados de pago*/
  get_estados_pago(req, res){
    res.status(200).json(Payment.schema.obj.users.type[0].state.enum);
  }
}
