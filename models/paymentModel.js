const mongoose = require('mongoose')
mongoose.set('useCreateIndex',true);
const Schema = mongoose.Schema
const ObjectId = mongoose.Schema.Types.ObjectId;

const paymentSchema = new Schema({
  name: {type:String, require: [true, 'Se tiene que mandar un name']},
  users: {
    type: [
      {
        user_id:{type:ObjectId},
        imgsrc:String,
        state:{type:String,enum:['Por pagar','En proceso','pagado']}
      }
    ],
    required: [true, 'se require mandar un usuario']
  },
  amount: { type: Number, required: [true, 'Se requiere una cantidad'] },
  description: {type:String, require:[true, 'Se requiere una description']},
  domiciliado:{type:String},
})

module.exports = mongoose.model('Payment', paymentSchema);
