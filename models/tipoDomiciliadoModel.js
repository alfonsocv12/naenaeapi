const mongoose = require('mongoose')
const Schema = mongoose.Schema

const tipoDomiciliadoModel = new Schema({
  name:{type:String, require:[true, 'Se necesita mandar un name']},
  segundos:{type:String, require:[true, 'Se necesita mandar los segundos']}
})

module.exports = mongoose.model('tipo_domiciliado', tipoDomiciliadoModel)
