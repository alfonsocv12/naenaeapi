const mongoose = require('mongoose')
const Schema = mongoose.Schema
// const ObjectId = mongoose.Schema.Types.ObjectId;

const houseSchema = new Schema({
    street: String,
    exteriorNumber: Number
});

module.exports = mongoose.model('house', houseSchema);
