const mongoose = require('mongoose')
const Schema = mongoose.Schema
const ObjectId = mongoose.Schema.Types.ObjectId;

const activitySchema = new Schema({
  name: {type:String, require:[true, 'Se necesita mandar un name']},
  user_id: {type:ObjectId, required: [true, 'se require mandar un user_id']},
  date: { type: Date, required:[true, 'Se require mandar date']},
  imgsrc:{type:String},
  state: {
    type: String,
    enum:['Agendado','En proceso','Pospuesto','finalizado'],
    default:"Agendado"
  },
  description:{type:String},
  capacity: { type: Number, required: true, min: 1 },
  peopleAttending: [ObjectId],
})

module.exports = mongoose.model('Activity', activitySchema)
