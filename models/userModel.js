const mongoose = require('mongoose')
const Schema = mongoose.Schema
const ObjectId = mongoose.Schema.Types.ObjectId

const userSchema = new Schema({
    username: { type: String, require: [true, "Necesita username"] },
    name: { type: String, require: [true, "Necesita nombre"] },
    lastname: { type: String, require: [true, "Necesita apellido"] },
    email: { type: String, require: [true, "Necesita correo"] },
    usertype: {
        type: String,
        enum: ['Residente', 'Guardia', 'Administrador', 'Trabajador'],
        default: "Residente"
    },
})

module.exports = mongoose.model('User', userSchema)