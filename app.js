const express = require("express"),
    app = express(),
    mongoose = require('mongoose')
require('dotenv').config();
const house = require('./routes/houses.routes');
app.listen(3000, function() {
    console.log('listening on port 3000!');
});

/*set db*/
mongoose.Promise = Promise
async function run() {
    await mongoose.connect(process.env.MONGODB_URI, {
        autoReconnect: true,
        reconnectTries: 1000000,
        reconnectInterval: 3000,
        useNewUrlParser: true,
        useUnifiedTopology: true,
        useFindAndModify: false
    })
}
run().catch(error => console.error(error))

/*Plugings*/
app.use(require('cors')());
app.use(require('body-parser').json());
app.use(require('body-parser').urlencoded({ extended: true }));
// app.use(require('method-override'));
// app.use(function (err, req, res, next) {
//   // console.log(req.error);
//   res.status(500).send({error:req.error});
// });

app.use('/', require("./routes/landingRoutes"));
app.use('/actividades', require("./routes/actividadesRoutes"));
app.use('/usuarios', require("./routes/usuariosRoutes"));
app.use('/pagos', require("./routes/pagosRoutes"));
app.use('/tipo_domicialiado', require("./routes/tipoDomiciliadoRoutes"));
app.use('/houses', house);